global css html
	ff:sans

import { HttpClient } from "./core/http"
import { Service } from "./core/services"

tag Canvas
	prop chart
	
	def render
		<self>
			<canvas$chartData width='100' height='100'>
	
	def rendered
		const c = new Chart($chartData, chart)

tag app	
	def render
		const httpClient = new HttpClient("http://localhost:5000")
		const report = 
			annualReports: await (await httpClient.get "/annual-reports").json!
			annualQuotationReports: await (await httpClient.get "/annual-quotation-reports").json!
			salesPeopleReports: await (await httpClient.get "/sales-people-reports").json!
			distributionAreaReports: await (await httpClient.get "/distribution-area-reports").json!
		const service = new Service(report)

		<self>
			<div[w:400px]>
				<Canvas chart=service.turnoverChart>
			<div[w:400px]>
				<Canvas chart=service.bidChart>
			<div[w:400px]>
				<Canvas chart=service.averageOrderChart>


imba.mount <app>