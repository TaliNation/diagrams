import { HttpClient } from './http'

def createLineChart chartData\any, title\string
	{
		type: "line"
		data: chartData
		options:
			responsive: true
			plugins:
				legend:
					position: "top"
				title:
					display: true
					text: title
	}

def createBarChart chartData\any, title\string
	{
		type: "bar"
		data: chartData,
		options:
			responsive: true
			plugins:
				legend:
					position: "top"
				title:
					display: true
					text: title
	}

class Service
	constructor report\any
		this.report = report

	get turnoverChart
		const data = 
			year: report.annualReports.map(do(d) d.year.value)
			turnover: report.annualReports.map(do(d) d.turnover.value)

		const chartData =
			labels: data.year
			datasets: [
				label: "Turnover"
				data: data.turnover
			]

		createBarChart(chartData, "Annual Turnover")

	get bidChart
		const data =
			year: report.annualReports.map(do(d) d.year.value)
			quotationTotal: report.annualReports.map(do(d) d.quotationTotal.value)

		const chartData =
			labels: data.year
			datasets: [
				label: "Total quotation value"
				data: data.quotationTotal
			]
		
		createBarChart(chartData, "Annual Quotation Value")

	get averageOrderChart
		const data =
			year: report.annualReports.map(do(d) d.year.value)
			medianSellingPrice: report.annualReports.map(do(d) d.medianSellingPrice.value)
			averageSellingPrice: report.annualReports.map(do(d) d.averageSellingPrice.value)

		const chartData =
			labels: data.year
			datasets: [
				{
					label: "Prix de vente moyen"
					data: data.averageSellingPrice
				}
				{
					label: "Prix de vente médian"
					data: data.medianSellingPrice
				}
			]
		
		createBarChart(chartData, "Prix de vente")



export { Service }