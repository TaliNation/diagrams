class HttpClient
	constructor rootUrl\string
		this.rootUrl = rootUrl

	def get url\string
		window.fetch (rootUrl + url)

	def post url\string, body\object
		window.fetch (rootUrl + url),
			method: 'POST'
			headers:
				Content-Type: 'application/json'
			body: body

	def delete url\string
		window.fetch (rootUrl + url),
			method: 'DELETE'

export { HttpClient }